<?php
//http://forum.dklab.ru/viewtopic.php?p=95077
class pdb {

    function _link($newLink=false) {
        static $link;
        if($newLink) $link = $newLink;
        return $link;
    }

    function _fixSql($args) {
        $sql = array_shift($args);
        if(count($args)) {
            $data = array_map(array('mysql','esc'), $args);
            $sql = str_replace(array('%','?'), array('%%','%s'), $sql);
            $sql = vsprintf($sql, $data);
        }
        return $sql;
    }

    function count($increment=false) {
        static $count = 0;
        if($increment) $count++;
        return $count;
    }

    function connect() {
        if($link=mysql_connect(DB_SERVER, DB_LOGIN, DB_PASSWORD)) pdb::_link($link);
        if(!@mysql_select_db(DB_DATABASE, pdb::_link()));
    }

    function disconnect() {
        mysql_close(mysql::_link());
    }

    function query() {
        pdb::count(1);
        $sql = pdb::_fixSql(func_get_args());
        if(!$query=mysql_query($sql, pdb::_link()));
        return new pdbResultSet($query);
    }

    function esc($str) {
        return mysql_escape_string(stripslashes($str));
    }

}

class pdbResultSet {

    var $q;

    function pdbResultSet($query) {
        $this->q = $query;
	//print $query;
    }

    function getRow() {
        return mysql_fetch_assoc($this->q);
    }

    function getVar() {
        return ($row=mysql_fetch_row($this->q)) ? $row[0] : false;
    }

    function getAll() {
        $all = false;
        while($row=$this->getRow()) $all[] = $row;
        return $all;
    }

    function getCol() {
        $col = false;
        while($var=$this->getVar()) $col[] = $var;
        return $col;
    }

    function rowCount() {
        return mysql_num_rows($this->q);
    }

    function insertId() {
        return mysql_insert_id($this->q);
    }

}