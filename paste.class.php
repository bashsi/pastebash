<?php
	class Paste {
		public $content = "";
		public $author = "";
		private $date = "";
		public $language = "";
		private $views = 0;
		private $password = "";
		private $id = 0;
		
		public function __construct($bNewPaste, $iPasteId) {
			$this->id = intval($iPasteId);
			if (!$bNewPaste && !$this->id) {
				throw new Exception("Paste id not specified");
			}
			if (!$bNewPaste) {
				$this->IncViews();
				$this->LoadPaste();
			}
		}
		
		private function IncViews() {
			$this->views++;
			global $db;
			$db->query("UPDATE `pastes` SET `views`='".$this->views."' WHERE `id`='".$this->id."' LIMIT 1;");
		}
		
		public function GetViews() {
			return $this->views;
		}
		
		private function LoadPaste() {
			global $db;
			$result = $db->query("SELECT * FROM `pastes` WHERE `id`='".$this->id."' LIMIT 1;")->getRow();
			$this->content = $result['content'];
			$this->author = $result['author'];
			$this->language = $result['language'];
			$this->password = $result['password'];
			$this->views = $result['views'];
			$this->date = $result['date'];
		}
		
		public function SetPassword($sPassword) {
			$this->password = hash('ripemd128', $sPassword);
			fnEncrypt($this->content,$this->password);
		}
		
		public function CheckPassword($uPassword) {
			if ($this->password) {
				if ($this->password==hash('ripemd128', $uPassword)) {
					fnDecrypt($this->content,$this->password);
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}
		
		public function Delete() {
			global $db;
			$db->query("DELETE FROM `pastes` WHERE `id`='".$thid->id."' LIMIT 1;");
		}
		
		public function Save() { 
			global $db;
			if (!$this->id) {
				$query = "INSERT INTO `pastes` (`content`, `language`, `author`, `password`, `views`) VALUES ('');";
				$db->query($query);
			} else {
				$query = "UPDATE `pastes` SET `content`='".$this->content."', `author`='".$this->author."', `language`='".$this->language."', `password`='".$this->password."' WHERE `id`='".$this-id."' LIMIT 1;";
				$db->query($query);
			}
		}
	}
?>